package com.example.showcase;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.Build;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;

import com.example.showcase.showCase.IAnimationFactory;
import com.example.showcase.showCase.IDetachedListener;
import com.example.showcase.showCase.IShowcaseListener;
import com.example.showcase.showCase.MaterialShowcaseView;
import com.example.showcase.showCase.PrefsManager;
import com.example.showcase.showCase.ShowcaseConfig;
import com.example.showcase.showCase.ShowcaseTooltip;
import com.example.showcase.showCase.shape.Shape;
import com.example.showcase.showCase.target.Target;
import com.example.showcase.showCase.target.ViewTarget;

import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.List;

public class CaseShowTest extends FrameLayout {

    public static final int DEFAULT_SHAPE_PADDING = 10;
    public static final int DEFAULT_TOOLTIP_MARGIN = 10;
    long DEFAULT_DELAY = 0;
    long DEFAULT_FADE_TIME = 300;

    private int mOldHeight;
    private int mOldWidth;
    private Bitmap mBitmap;// = new WeakReference<>(null);
    private Canvas mCanvas;
    private Paint mEraser;
    private Target mTarget;
    private Shape mShape;
    private int mXPosition;
    private int mYPosition;
    private boolean mWasDismissed = false, mWasSkipped = false;
    private int mShapePadding = DEFAULT_SHAPE_PADDING;
    private int tooltipMargin = DEFAULT_TOOLTIP_MARGIN;

    private View mContentBox;
    private TextView mTitleTextView;
    private TextView mContentTextView;
    private TextView mDismissButton;
    private boolean mHasCustomGravity;
    private TextView mSkipButton;
    private int mGravity;
    private int mContentBottomMargin;
    private int mContentTopMargin;
    private int mContentRightMargin;
    private int mContentLeftMargin;
    private boolean mDismissOnTouch = false;
    private boolean mShouldRender = false; // flag to decide when we should actually render
    private boolean mRenderOverNav = false;
    private int mMaskColour;
    private IAnimationFactory mAnimationFactory;
    private boolean mShouldAnimate = true;
    private boolean mUseFadeAnimation = false;
    private long mFadeDurationInMillis = DEFAULT_FADE_TIME;
    private Handler mHandler;
    private long mDelayInMillis = DEFAULT_DELAY;
    private int mBottomMargin = 0;
    private boolean mSingleUse = false; // should display only once
    private PrefsManager mPrefsManager; // used to store state doe single use mode
    List<IShowcaseListener> mListeners; // external listeners who want to observe when we show and dismiss
    private UpdateOnGlobalLayout mLayoutListener;
    private IDetachedListener mDetachedListener;
    private boolean mTargetTouchable = false;
    private boolean mDismissOnTargetTouch = true;

    private boolean isSequence = false;

    private ShowcaseTooltip toolTip;
    private boolean toolTipShown;

    public CaseShowTest(@NonNull Context context) {
        super(context);
        init(context);
    }

    public CaseShowTest(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CaseShowTest(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public CaseShowTest(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context);
    }

    private void init(final Context context) {
        setWillNotDraw(false);

        mListeners = new ArrayList<>();

        // make sure we add a global layout listener so we can adapt to changes
        mLayoutListener = new UpdateOnGlobalLayout();
        getViewTreeObserver().addOnGlobalLayoutListener(mLayoutListener);

        // consume touch events
//        setOnTouchListener(this);

        mMaskColour = Color.parseColor(ShowcaseConfig.DEFAULT_MASK_COLOUR);
        setVisibility(INVISIBLE);


        View contentView = LayoutInflater.from(getContext()).inflate(R.layout.showcase_content_test_bottom, this, true);
        contentView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
//                hide();
            }
        });
        mContentBox = contentView.findViewById(R.id.content_box);
        mTitleTextView = contentView.findViewById(R.id.tv_title);
        mDismissButton = contentView.findViewById(R.id.tv_dismiss);
        mContentTextView = contentView.findViewById(R.id.tv_content);

        mSkipButton = contentView.findViewById(R.id.tv_skip);

    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // don't bother drawing if we're not ready
//        if (!mShouldRender) return;

        // get current dimensions
        final int width = getMeasuredWidth();
        final int height = getMeasuredHeight();

        // don't bother drawing if there is nothing to draw on
        if (width <= 0 || height <= 0) return;

        // build a new canvas if needed i.e first pass or new dimensions
        if (mBitmap == null || mCanvas == null || mOldHeight != height || mOldWidth != width) {

            if (mBitmap != null) mBitmap.recycle();

            mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);

            mCanvas = new Canvas(mBitmap);
        }

        // save our 'old' dimensions
        mOldWidth = width;
        mOldHeight = height;

        // clear canvas
        mCanvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);

        // draw solid background
        mCanvas.drawColor(mMaskColour);

        // Prepare eraser Paint if needed
        if (mEraser == null) {
            mEraser = new Paint();
            mEraser.setColor(0xFFFFFFFF);
            mEraser.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
            mEraser.setFlags(Paint.ANTI_ALIAS_FLAG);
        }

        // draw (erase) shape
//        mShape.draw(mCanvas, mEraser, mXPosition, mYPosition);

        // Draw the bitmap on our views  canvas.
        canvas.drawBitmap(mBitmap, 0, 0, null);
    }


    public boolean show(final Activity activity) {

        /**
         * if we're in single use mode and have already shot our bolt then do nothing
         */
//        if (mSingleUse) {
//            if (mPrefsManager.hasFired()) {
//                return false;
//            } else {
//                mPrefsManager.setFired();
//            }
//        }

        ((ViewGroup) activity.getWindow().getDecorView()).addView(this);

        setShouldRender(true);


        if (toolTip != null) {

            if (!(mTarget instanceof ViewTarget)) {
                throw new RuntimeException("The target must be of type: " + ViewTarget.class.getCanonicalName());
            }

            ViewTarget viewTarget = (ViewTarget) mTarget;

            toolTip.configureTarget(this, viewTarget.getView());

        }


        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                boolean attached;
                // taken from https://android.googlesource.com/platform/frameworks/support/+/refs/heads/androidx-master-dev/core/src/main/java/androidx/core/view/ViewCompat.java#3310
                if (Build.VERSION.SDK_INT >= 19) {
                    attached = isAttachedToWindow();
                } else {
                    attached = getWindowToken() != null;
                }
                if (mShouldAnimate && attached) {
//                    fadeIn();
                } else {
                    setVisibility(VISIBLE);
//                    notifyOnDisplayed();
                }
            }
        }, mDelayInMillis);

//        updateDismissButton();

        return true;
    }


    private void setShouldRender(boolean shouldRender) {
        mShouldRender = shouldRender;
    }


    public class UpdateOnGlobalLayout implements ViewTreeObserver.OnGlobalLayoutListener {

        @Override
        public void onGlobalLayout() {
//            setTarget(mTarget);
        }
    }
}
