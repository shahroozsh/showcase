package com.example.showcase.showCase;


public interface IDetachedListener {
    void onShowcaseDetached(MaterialShowcaseView showcaseView, boolean wasDismissed, boolean wasSkipped);
}
